Table of Contents

Creational

    Constructor
    Factory
    Singleton

Structural

    Adapter
    Decorator
    Facade
    Proxy

Behavioral

    Observer
    Strategy
    Template